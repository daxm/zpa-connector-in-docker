#!/usr/bin/env bash

echo "--> Pulling images:"
docker-compose pull
echo

echo "--> Updating GIT Repo"
git pull
echo

echo "--> Start docker-compose build and launch containers if successful:"
docker-compose up --remove-orphans --build -d
echo

echo "--> Prune Images:"
docker image prune -a -f
echo

echo "--> Docker Status"
docker ps
echo

#echo "--> Logs for these containers.  User ctrl-c to exit."
#docker-compose logs -f
#echo
