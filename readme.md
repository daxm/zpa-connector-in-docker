# ZPA Connector as Docker Container

1. Edit the .env file and give your container a name and set the provisioning key.
2. Use `runme.sh` to pull, update, and run container.
3. Issue `docker ps` to view container status.
